# ics-ans-role-rsync-module-backup

Ansible role to backup data through rsync module.

## Role Variables

```yaml
---
rsync_module_backup_server: learning-nas.cslab.esss.lu.se
rsync_module_backup_src: /tmp/
rsync_module_backup_pool: backup
rsync_module_backup_module_name: "{{ ansible_hostname }}"
rsync_module_backup_mounted_folder: "{{ rsync_module_backup_pool }}/{{ ansible_hostname }}"
rsync_module_backup_dest: "rsync://{{ rsync_module_backup_server }}/{{ rsync_module_backup_module_name }}"
rsync_module_backup_pre_scripts_path: "/usr/local/bin/prescript/"
rsync_module_backup_pre_scripts: false
rsync_module_backup_post_scripts_path: "/usr/local/bin/prescript/"
rsync_module_backup_post_scripts: false
rsync_module_backup_snapshot_name: "none"
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-rsync-module-backup
```

## License

BSD 2-clause
